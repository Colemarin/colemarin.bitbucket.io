var PracticeFrontend_8py =
[
    [ "getData", "PracticeFrontend_8py.html#aaf51d403951b929e4a00a2aa774bb784", null ],
    [ "kb_cb", "PracticeFrontend_8py.html#ac3921466b8caa381bbc6403bf0396022", null ],
    [ "baudrate", "PracticeFrontend_8py.html#aa61f796c85582d0aec49419836ad183e", null ],
    [ "last_key", "PracticeFrontend_8py.html#a46a4e8e7676139cd2c5db1ed49844875", null ],
    [ "N", "PracticeFrontend_8py.html#adb5ea69ace9e774cec3c2c5a7b79d1d0", null ],
    [ "newline", "PracticeFrontend_8py.html#a642644fe14aa7ee875573cc4fe92dfd8", null ],
    [ "newValues", "PracticeFrontend_8py.html#a82d0e9265a735417851514a131386c10", null ],
    [ "port", "PracticeFrontend_8py.html#a7a12dc33256af4a43d7b2e2e6b510900", null ],
    [ "splitStrings", "PracticeFrontend_8py.html#ac429172eed7fd7f6fc45c24f9bcad592", null ],
    [ "strippedString", "PracticeFrontend_8py.html#a271173613c3020335fe50b286f514868", null ],
    [ "timeout", "PracticeFrontend_8py.html#a6791e11b9104e6b670746be908ca5aeb", null ],
    [ "user_in", "PracticeFrontend_8py.html#a1c855c99330bf431cff16939af6054f0", null ],
    [ "writing", "PracticeFrontend_8py.html#a808774a9c43a88f0031eba046d1ca17a", null ],
    [ "x_vals", "PracticeFrontend_8py.html#acf1228a99e23ac2f0060e15f2e7e418c", null ],
    [ "y_vals", "PracticeFrontend_8py.html#af107d6c74e95723a0965d7abd51add2e", null ]
];