var files_dup =
[
    [ "BlinkingLight.py", "BlinkingLight_8py.html", "BlinkingLight_8py" ],
    [ "ControllerDriver.py", "ControllerDriver_8py.html", [
      [ "ClosedLoop", "classControllerDriver_1_1ClosedLoop.html", "classControllerDriver_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", [
      [ "ControllerTask", "classControllerTask_1_1ControllerTask.html", "classControllerTask_1_1ControllerTask" ]
    ] ],
    [ "Elevator.py", "Elevator_8py.html", "Elevator_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Encoder_Frontend.py", "Encoder__Frontend_8py.html", "Encoder__Frontend_8py" ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", [
      [ "Motor_Driver", "classMotor__Driver_1_1Motor__Driver.html", "classMotor__Driver_1_1Motor__Driver" ]
    ] ],
    [ "PracticeBackend.py", "PracticeBackend_8py.html", [
      [ "dataCollection", "classPracticeBackend_1_1dataCollection.html", "classPracticeBackend_1_1dataCollection" ]
    ] ],
    [ "PracticeFrontend.py", "PracticeFrontend_8py.html", "PracticeFrontend_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares", "classshares_1_1shares.html", "classshares_1_1shares" ]
    ] ],
    [ "simonSaysGame.py", "simonSaysGame_8py.html", "simonSaysGame_8py" ],
    [ "UI_Task.py", "UI__Task_8py.html", [
      [ "UI_Task", "classUI__Task_1_1UI__Task.html", "classUI__Task_1_1UI__Task" ]
    ] ]
];