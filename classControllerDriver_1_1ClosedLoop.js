var classControllerDriver_1_1ClosedLoop =
[
    [ "__init__", "classControllerDriver_1_1ClosedLoop.html#a47fed694fed715ef6b3a97c9b87722e8", null ],
    [ "get_Kp", "classControllerDriver_1_1ClosedLoop.html#aa9b0045b3858f206d2aa5235b662bf42", null ],
    [ "runP", "classControllerDriver_1_1ClosedLoop.html#a26097741983edaf347b965536196830e", null ],
    [ "runPI", "classControllerDriver_1_1ClosedLoop.html#a845290a0363ce1a605a847edcd78bd1e", null ],
    [ "set_Kp", "classControllerDriver_1_1ClosedLoop.html#a630ac599e681037f7c9b8a9a8cb8819c", null ],
    [ "J", "classControllerDriver_1_1ClosedLoop.html#a34f1fd480075242220a8ef4cfd9b848c", null ],
    [ "K", "classControllerDriver_1_1ClosedLoop.html#a7803a50a90b40a63b194629f5831a63c", null ],
    [ "Kp", "classControllerDriver_1_1ClosedLoop.html#a4730a989bb81f374bd055ca08c3ab0a3", null ],
    [ "L", "classControllerDriver_1_1ClosedLoop.html#aa6d09d94f7ea00458a73edb9fe439fe2", null ],
    [ "lastJ", "classControllerDriver_1_1ClosedLoop.html#a9da71bbf65f734041b66464b00a91691", null ],
    [ "nowJ", "classControllerDriver_1_1ClosedLoop.html#a8ea1eceb70aa939cc22c2f439d255a4e", null ]
];