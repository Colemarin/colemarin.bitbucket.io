var searchData=
[
  ['get_5fdata_134',['get_data',['../Encoder__Frontend_8py.html#a88fb3945a4d719972172b4314d79ebec',1,'Encoder_Frontend']]],
  ['get_5fdelta_135',['get_delta',['../classEncoder_1_1Encoder.html#a7bf293682012aeef8fd2e12ae9deb383',1,'Encoder::Encoder']]],
  ['get_5fkp_136',['get_Kp',['../classControllerDriver_1_1ClosedLoop.html#aa9b0045b3858f206d2aa5235b662bf42',1,'ControllerDriver::ClosedLoop']]],
  ['get_5fposition_137',['get_position',['../classEncoder_1_1Encoder.html#a756e2d8ed0343f3909fd2665d9b56331',1,'Encoder::Encoder']]],
  ['get_5fvelocity_138',['get_velocity',['../classEncoder_1_1Encoder.html#af23856c366e8c9f0e3b6399e8ae67e5c',1,'Encoder::Encoder']]],
  ['getdata_139',['getData',['../PracticeFrontend_8py.html#aaf51d403951b929e4a00a2aa774bb784',1,'PracticeFrontend']]]
];
