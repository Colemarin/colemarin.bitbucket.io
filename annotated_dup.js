var annotated_dup =
[
    [ "ControllerDriver", null, [
      [ "ClosedLoop", "classControllerDriver_1_1ClosedLoop.html", "classControllerDriver_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask", null, [
      [ "ControllerTask", "classControllerTask_1_1ControllerTask.html", "classControllerTask_1_1ControllerTask" ]
    ] ],
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "Motor_Driver", "classMotor__Driver_1_1Motor__Driver.html", "classMotor__Driver_1_1Motor__Driver" ]
    ] ],
    [ "PracticeBackend", null, [
      [ "dataCollection", "classPracticeBackend_1_1dataCollection.html", "classPracticeBackend_1_1dataCollection" ]
    ] ],
    [ "shares", null, [
      [ "shares", "classshares_1_1shares.html", "classshares_1_1shares" ]
    ] ],
    [ "simonSaysClass", null, [
      [ "simonSaysClass", "classsimonSaysClass_1_1simonSaysClass.html", "classsimonSaysClass_1_1simonSaysClass" ]
    ] ],
    [ "UI_Task", null, [
      [ "UI_Task", "classUI__Task_1_1UI__Task.html", "classUI__Task_1_1UI__Task" ]
    ] ]
];