var classshares_1_1shares =
[
    [ "__init__", "classshares_1_1shares.html#a1638f1443de438c626c309024115330c", null ],
    [ "controller", "classshares_1_1shares.html#a2b175a612ed7d5b0821d4e2b0e573a54", null ],
    [ "delta", "classshares_1_1shares.html#aaa4583605ff090324746269b55d36680", null ],
    [ "motoff", "classshares_1_1shares.html#a4f012fe2ff031f8a010a8bad4ba823c1", null ],
    [ "moton", "classshares_1_1shares.html#aceb6b6fa20d32056d41ea7be89df098d", null ],
    [ "position", "classshares_1_1shares.html#a7d7b8546129523a96bb8bdb0a21e9177", null ],
    [ "setter", "classshares_1_1shares.html#a55e357672f90fb4adc71e1300551fd92", null ],
    [ "velocity", "classshares_1_1shares.html#a68379e6db274ab54373803caa84bcc8e", null ],
    [ "ctrlTask", "classshares_1_1shares.html#a95ce5f32f4e972b79a485a2f562f7729", null ],
    [ "nowDelta", "classshares_1_1shares.html#ab924667df006b276cf3a03e836a3930e", null ],
    [ "nowPos", "classshares_1_1shares.html#a3528ff0bfc552ecd38fd3a2ff3fef030", null ],
    [ "nowSet", "classshares_1_1shares.html#ad4e15d6bd581ff338f6e6c6806be19f4", null ],
    [ "nowVelocity", "classshares_1_1shares.html#a783974af3c171458db2a04dcd838e331", null ],
    [ "offMot", "classshares_1_1shares.html#abcc22ff57b4686e15acb997376a2d6b5", null ],
    [ "onMot", "classshares_1_1shares.html#ac89a60003663467af352366628d2b735", null ],
    [ "returnData", "classshares_1_1shares.html#abcd68c2c09a7cc60605f6839d6edaf30", null ]
];